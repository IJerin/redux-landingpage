import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  Carousel: {
    flexDirection: 'column',
    backgroundColor: 'white'
  },
  featureText: {
    fontFamily: 'open-sans.bold',
    fontSize: 18,
    color: 'rgba(51,51,51,1)',
  },
  viewText: {
    fontFamily: 'open-sans.semibold',
    fontSize: 14,
    color: 'rgba(27, 172, 149, 1)',
  },
  row1: {
    flexDirection:'row',
    paddingTop: 15,
    marginLeft: 24,
    marginRight: 24
  },
  row2: {
    flexDirection:'row',
    paddingTop: 33,
    paddingBottom: 10,
    marginLeft: 24,
    marginRight: 24
  },
  page: {
    height: 120,
    width: 176,
    borderRadius: 5
  },
  CarouselPager: {
    flexDirection: 'row',
    marginLeft: -80,
  },
  picBoldText: {
    fontFamily: 'open-sans.bold',
    fontSize: 14,
    color: 'rgba(51,51,51,1)',
  },
  picSmallText: {
    fontFamily: 'open-sans.regular',
    fontSize: 12,
    color: 'rgba(118,118,118,1)',
  },
  footerContainer: {
    flexDirection: 'row',
    paddingLeft:48,
    paddingRight: 48,
    paddingTop: '20%',
  },
  footerIcon: {
    flexDirection:'column',
    width:'25%',
    alignItems:'center'
  }
})
