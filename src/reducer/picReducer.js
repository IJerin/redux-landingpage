import { PIC_1, PIC_2, PIC_3, PIC_4 } from '../actions/types'

const initialState = {
  pic: require('../Images/greyPhoto.png')
}

export default (state = initialState, action) => {
  switch (action.type) {
    case PIC_1:
      return {pic: require('../Images/greyPhoto.png')}
    case PIC_2:
      return {pic: require('../Images/colors_yellow-01.png')};
      case PIC_3:
        return {pic: require('../Images/greyPhoto.png')};
        case PIC_4 :
          return {pic: require('../Images/colors_yellow-01.png')};
    default:
      return state;
  }
}
