import React, { Component } from "react";
import { AppRegistry, Text, TextInput, View, Button, Image, TouchableOpacity } from "react-native";
import CarouselPager from 'react-native-carousel-pager';
import { connect } from 'react-redux';
import { pic_1, pic_2, pic_3, pic_4 } from './actions';
import styles from './styles';

class App extends Component {
  constructor(props) {
    super(props);
    this.state
    this.onChangeText = this.onChangeText.bind(this);
  }

  onChangeText(number) {
    const count = parseInt(number);
    this.props.counterSet(count)
  }
  onClickSomething() {
   this.carousel.goToPage(0);
 }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.row1}>
          <View style={{flexDirection: 'column', width: '30%'}}>
          </View>
          <View style={{flexDirection: 'column', width: '64%',}}>
            <Image style={{height:31, width:154}} source={ require('./Images/view_logo.png')}/>
          </View>
          <View style={{flexDirection: 'column', paddingTop: 15}}>
            <Image style={{height:14, width:20}} source={ require('./Images/menu.png')}/>
          </View>
        </View>
        <View style={{paddingTop:40,alignItems: 'center',paddingRight:24, paddingLeft: 24}}>
          <Image style={{height:220, width: '100%', borderRadius: 4}} source={this.props.picLoc.pic}/>
        </View>
        <View style={styles.row2}>
          <View style={{flexDirection: 'column', width: '85%'}}>
            <Text style={styles.featureText}>Featured Guides</Text>
          </View>
          <View style={{flexDirection: 'column', paddingTop: 5}}>
            <Text style={styles.viewText}>View all</Text>
          </View>
        </View>
      <View style={styles.Carousel}>
        <View style={styles.CarouselPager}>
          <CarouselPager
            containerPadding={100}
              pageSpacing={-102}
              blurredZoom={1}
              blurredOpacity={1}
            >
              <View key={'page0'}>
                <TouchableOpacity onPress={this.props.pic_1}>
                  <Image style={styles.page} source={ require('./Images/greyPhoto.png')}/>
                </TouchableOpacity>
                <Text style={styles.picBoldText}>
                  Iconic San Francisco
                </Text>
                <Text style={styles.picSmallText}>
                  San Francisco, CA
                </Text>
              </View>
              <View key={'page1'}>
                <TouchableOpacity onPress={this.props.pic_2}>
                  <Image style={styles.page} source={ require('./Images/colors_yellow-01.png')}/>
                </TouchableOpacity>
                <Text style={styles.picBoldText}>
                  Iconic San Francisco
                </Text>
                <Text style={styles.picSmallText}>
                  San Francisco, CA
                </Text>
              </View>
              <View key={'page2'}>
                <TouchableOpacity onPress={this.props.pic_3}>
                  <Image style={styles.page} source={ require('./Images/greyPhoto.png')}/>
                </TouchableOpacity>
                <Text style={styles.picBoldText}>
                  Iconic San Francisco
                </Text>
                <Text style={styles.picSmallText}>
                  San Francisco, CA
                </Text>
              </View>
              <View key={'page3'}>
                <TouchableOpacity onPress={this.props.pic_4}>
                  <Image style={styles.page} source={ require('./Images/colors_yellow-01.png')}/>
                </TouchableOpacity>
                <Text style={styles.picBoldText}>
                  Iconic San Francisco
                </Text>
                <Text style={styles.picSmallText}>
                  San Francisco, CA
                </Text>
              </View>

          </CarouselPager>
        </View>
      </View>
      <View style={styles.footerContainer}>
        <View style={styles.footerIcon}>
          <Image style={{height:20, width:20}} source={ require('./Images/home_icon.png')}/>
        </View>
        <View style={styles.footerIcon}>
          <Image style={{height:20, width:20}} source={ require('./Images/search_icon.png')}/>
        </View>
        <View style={styles.footerIcon}>
          <Image style={{height:20, width:20}} source={ require('./Images/heart_icon.png')}/>
        </View>
        <View style={styles.footerIcon}>
          <Image style={{height:20, width:20}} source={ require('./Images/person_icon.png')}/>
        </View>
      </View>
    </View>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    picLoc: state
  }
};

export default connect(mapStateToProps, { pic_1, pic_2, pic_3, pic_4 })(App);
