import { PIC_1, PIC_2, PIC_3, PIC_4 } from './types'

export const pic_1 = () => {
  return {
    type: PIC_1
  };
}
export const pic_2 = () => {
  return {
    type: PIC_2
  };
}
export const pic_3 = () => {
  return {
    type: PIC_3
  };
}
export const pic_4 = () => {
  return {
    type: PIC_4,
  };
}
