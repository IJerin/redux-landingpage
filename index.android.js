import { AppRegistry } from "react-native";
import React from 'react';
import { Provider } from 'react-redux';
import App from "./src/app";
import { createStore } from 'redux';
import reducer from './src/reducer/picReducer';
import { connect } from 'react-redux';

const store = createStore(reducer);

const appRegister = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

AppRegistry.registerComponent("reduxlearningstarter", () => appRegister);
